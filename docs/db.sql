-- phpMyAdmin SQL Dump
-- version 3.3.2deb1ubuntu1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Erstellungszeit: 30. Juli 2013 um 10:51
-- Server Version: 5.1.67
-- PHP-Version: 5.3.2-1ubuntu4.19

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Datenbank: `projects_schwimmen`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `registrations`
--

CREATE TABLE IF NOT EXISTS `registrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` tinytext NOT NULL,
  `name` tinytext NOT NULL,
  `parent` tinyint(1) NOT NULL,
  `parent_firstname` tinytext NOT NULL,
  `parent_name` tinytext NOT NULL,
  `sex` enum('m','w') NOT NULL DEFAULT 'm',
  `street` tinytext NOT NULL,
  `city` tinytext NOT NULL,
  `postcode` varchar(10) NOT NULL,
  `birthday` date NOT NULL,
  `email` tinytext,
  `phone` tinytext,
  `size` enum('S','M','L','XL','XXL') NOT NULL,
  `route` enum('250','400','1600','4000') NOT NULL,
  `ak` int(1) NOT NULL,
  `age` int(3) NOT NULL,
  `paid` date NOT NULL DEFAULT '0000-00-00',
  `registration` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `key` int(11) DEFAULT NULL,
  `deliveried` datetime DEFAULT NULL,
  `signed` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

