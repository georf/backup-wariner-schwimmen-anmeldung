<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<title>unbenannt</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<meta name="generator" content="Geany 0.16" />
</head>

<body>
<?php

error_reporting(E_ALL);


if(isset($_FILES['uploaded'])){
	if(move_uploaded_file($_FILES['uploaded']['tmp_name'],"actual.csv")) {
		$arr = csv2sql("actual.csv","|","mgv_results");
		echo "<pre>";
		foreach($arr as $a) echo $a."\n";
		echo "</pre>";
		echo "<p><a href=\"./\">Zurück</a></p>";
	}
} else {
	echo "<ul>";
	echo "<li>Sicherheitskopie der Mappe anlegen</li>";
	echo "<li>Alle, die nicht angekommen sind löschen</li>";
	echo "<li>Benötigte Spalten: name vorname ort sex alter ak time (andere Löschen und diese so umbenennen)</li>";
	echo "<li>Die Zeit steht entweder als Zahl da(2043 für 20 Minuten und 43 Sekunden) oder mit Doppelpunkt (01:22:21 für 1 Stunde, 22 Minuten und 21 Sekunden)</li>";
	echo "<li>Die AK ist entweder nur eine Zahl oder es steht AK3 (also ich fische mit REG /[^0-9]*([0-9]+)[^0-9]*/ )</li>";
	echo "<li>In OO Calc: Speichern unter -> als CSV, Feldseperator ist die Pipe, also |, Textseperator ist nichts, also leer</li>";
	echo "<li>Hier Uploaden, danach prüfen ob Syntax richtig und alles normal ist</li>";
	echo "<li>Den <b>Defaultwert in der Tabelle</b> für das Jahr anpassen (z.B. 25 für 2010)</li>";
	echo "<li>Einfügen in Tabelle usr_web8_6.mgv_results</li>";
	echo "<li>Das ganze macht man mit alle Mappen in der Datei, also für alle Altersklassen</li>";
	echo "</ul>";

	echo "<form enctype=\"multipart/form-data\" action=\"\" method=\"post\">";
	echo "<p>File:<input name='uploaded' type='file'/><input type='submit' value='Upload'/></p>";
	echo "</form>";
}

?>
</body>
</html>


<?php

function csv2sql($csvfile,$delimitatore,$nometable) {
	$csvhandle = file($csvfile);                    //Apro e leggo il contenuto del file csv e lo metto in un vettore
	$field = explode($delimitatore,$csvhandle[0]);       //leggo i campi sul primo elemento del vettore
	$campi = "";

	$time = -1;
	$ak = -1;

	for ($i = 0; $i < count($field); $i++) {
		$campi .= "`".trim($field[$i])."`,";
		if ( trim($field[$i]) == "time" ) {
			$time = $i;
		} elseif ( trim($field[$i]) == "ak" ) {
			$ak = $i;
		}
	}

	$campi = trim(substr($campi,0,-1)); // Komma wegnehmen

	for ($i=1;$i<count($csvhandle);$i++) {
		$valori = explode($delimitatore,$csvhandle[$i]);
		$values = "";
		for ($z = 0; $z < count($valori); $z++) {
			$val=trim($valori[$z]);
			if($time == $z) {
				$val = str_replace(":","",$val);
			} elseif($ak == $z) {
				if (preg_match("/[^0-9]*([0-9]+)[^0-9]*/",$val,$a)) {
					$val = $a[1];
				}
			}

			if (eregi("NULL",$val) == 0) {
				$values .= "'".str_replace("'","\'",$val)."',";
			} else {
				$values .= "NULL,";
			}
		}
		$values = trim(substr($values,0,-1)); // Komma wegnehmen
		$query = "INSERT INTO `".$nometable."` (".$campi.") VALUES (".trim($values).");";
		$_QUERY[]=$query;
	}
	return $_QUERY;

}

?>
