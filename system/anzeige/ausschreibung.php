<h1>Ausschreibung 2012</h1>
<h2>27. Langstreckenschwimmen<br/>"Großer Wariner See"<br/>04. August 2012</h2>
<table><tr><td class="ausschreibung-left">Veranstalter:</td><td>DLRG Ortverband Warin e.V.</td></tr>
<tr><td class="ausschreibung-left">Verantwortlicher Leiter:</td><td>Dietlind Schieweck<br/>Vorsitzende DLRG Ortverband Warin e.V.</td></tr>
<tr><td class="ausschreibung-left">Termin:</td><td>Sonnabend der 04. August 2012</td></tr>
<tr><td class="ausschreibung-left">Ort:</td><td>Strandbad Groß Wariner See - 19417 Warin</td></tr>
<tr><td class="ausschreibung-left">Strecke - Kinder:</td><td>250 m - AK1<br/>400 m - AK2 um den Pokal der Schweriner Volkszeitung<br/><p class="wichtig">Achtung: Kinder und Jugendliche bis zum 17. Lebensjahr dürfen nur mit schriftlicher Genehmigung der Erziehungsberechtigten starten!</p>
</td></tr>
<tr><td class="ausschreibung-left">Strecke - Erwachsene:</td><td>1600 m - AK3 bis AK5 um den Wanderpokal des Bürgermeisters<br/>4000 m - ohne AK</td></tr>
<tr><td class="ausschreibung-left">Altersklassen:</td><td>AK1: bis 10 Jahre<br/>AK2: 11 - 13 Jahre<br/>AK3: 14 - 27 Jahre<br/>AK4: 28 - 45 Jahre<br/>AK5: 46 Jahre und älter<br/><br/>Die Wertung erfolgt in den Altersklassen geschlechtlich getrennt.</td></tr>
<tr><td class="ausschreibung-left">Startzeiten:</td><td><table><tr><td class="c80">Kinder:</td><td>15 Uhr</td></tr>
<tr><td class="c80">Erwachsene:</td><td>16 Uhr</td></tr>
</table>
</td></tr>
<tr><td class="ausschreibung-left">Startgebühren:</td><td><table><tr><td class="c100">AK1, AK2:</td><td>5 Euro / am Tag der Veranstaltung 5 Euro</td></tr>
<tr><td class="c100">AK3, AK4, AK5:</td><td>15 Euro / am Tag der Veranstaltung 15 Euro</td></tr>
</table>
</td></tr>
<tr><td class="ausschreibung-left">Meldeschluss:</td><td>Tag der Veranstaltung um 14 Uhr!</td></tr>
</table>

