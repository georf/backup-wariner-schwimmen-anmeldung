<?php


require_once(__DIR__.'/inc/init.php');


include(__DIR__.'/templates/anzeige-header.php');

if (isset($_GET['page'])) {
	$_page = $_GET['page'];
} else {
	$_page = 'ausschreibung';
}

$vz = opendir('anzeige');
while ($file = readdir($vz)) {
	if (is_file('anzeige/'.$file) && $file == $_page.'.php') {
		include(__DIR__.'/anzeige/'.$file);
		break;
	}
}
closedir($vz);


include(__DIR__.'/templates/anzeige-footer.php');
