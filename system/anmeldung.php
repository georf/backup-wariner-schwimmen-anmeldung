<?php


require_once(__DIR__.'/inc/init.php');


include(__DIR__.'/templates/header.php');

if (isset($_GET['page'])) {
	$_page = $_GET['page'];
} else {
	$_page = 'select';
}

$vz = opendir('pages');
while ($file = readdir($vz)) {
	if (is_file('pages/'.$file) && $file == $_page.'.php') {
		include(__DIR__.'/pages/'.$file);
		break;
	}
}
closedir($vz);


include(__DIR__.'/templates/footer.php');
