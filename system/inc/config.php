<?php

$config = array();
$config['database']['server'] = 'localhost';
$config['database']['username'] = 'schwimmen';
$config['database']['password'] = 'brBzBedEn49sLxYB';
$config['database']['database'] = 'schwimmen';


$lss['date'] 		= strtotime('2013-08-03');
$lss['date_open']	= strtotime('2013-01-01');
$lss['number']		= 28;
$lss['close_days']	= 2;
$lss['big_money']	= 18;
$lss['small_money']	= 5;
$lss['table']		= 'registration_lss_2014';
$lss['mail-list']	= array('admin@warin.dlrg.de','schatzmeister@warin.dlrg.de');
$lss['file']		= 'anmeldung13.pdf';
$lss['sizes']		= array('S','M','L','XL','XXL');
$lss['routes']		= array(
						'0' => array(
							'name' => '250 Meter (AK 1)',
							'db' => '250',
							'max' => -1,
							'money' => 5,
						),
						'1' => array(
							'name' => '400 Meter (AK 2)',
							'db' => '400',
							'max' => -1,
							'money' => 5,
						),
						'2' => array(
							'name' => '1600 Meter (AK 3-5)',
							'db' => '1600',
							'max' => 250,
							'money' => 15,
						),
						'3' => array(
							'name' => '4000 Meter',
							'db' => '4000',
							'max' => 30,
							'money' => 15,
						),
);


$data = array(
	'parent_firstname'	=> array('Vorname',			'Test::tstring',		false),
	'parent_name'		=> array('Name',			'Test::tstring',		false),
	'parent'		=> array('Eltern',			'Test::boolean',		true),
	'firstname'			=> array('Vorname',			'Test::tstring',		true),
	'name'				=> array('Name',			'Test::tstring',		true),
	'sex'				=> array('Geschlecht',		'Test::sex',		true),
	'street'			=> array('Straße und Nr.',	'Test::tstring',		true),
	'postcode'			=> array('PLZ',				'Test::postcode',	true),
	'city'				=> array('Stadt',			'Test::tstring',		true),
	'size'			=> array('T-Shirt-Größe',	'Test::tee',		true),
	'route'				=> array('Strecke',			'Test::route',		true),
	'key'				=> array('Startnummer',			'Test::number',		true),
	'age'				=> array('Alter',			'Test::number',		true),
	'ak'				=> array('AK',			'Test::number',		true),
);

$config['lss'] = $lss;

$config['table-fields'] = array(
	'firstname',
	'name',
	'parent',
	'parent_firstname',
	'parent_name',
	'sex',
	'street',
	'city',
	'postcode',
	'birthday',
	'email',
	'phone',
	'size',
	'route',
	'ak',
	'age',
	'paid',
	'registration',
	'key',
	'deliveried',
	'signed',
);
