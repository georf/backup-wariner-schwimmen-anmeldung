<?php

function __autoload($className)
{
    $className = ltrim($className, '\\');
    $fileName = '';
    $namespace = '';
    if ($lastNsPos = strripos($className, '\\')) {
        $namespace = substr($className, 0, $lastNsPos);
        $className = substr($className, $lastNsPos + 1);
        $fileName = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
    }
    $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';

    require __DIR__.'/classes/'.$fileName;
}

// include needed files
require_once(__DIR__.'/config.php');

// create default output
$jsonOutput					= array();
$jsonOutput['warnings']		= array();
$jsonOutput['debug']		= array();


// try to connect to database
try {
	global $db;

	$db = new Database(
		$config['database']['server'],
		$config['database']['database'],
		$config['database']['username'],
		$config['database']['password']
	);

} catch (Exception $e) {
	echo $e->getMessage();
}
