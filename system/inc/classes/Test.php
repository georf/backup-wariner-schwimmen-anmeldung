<?php

class Test {

	static function tstring ($input, $name) {
		if ($input == '') {
			return 'Bitte geben Sie das Feld <i>%s</i> an.';
		} elseif (strlen($input) > 255) {
			return 'Die Eingabe im Feld <i>%s</i> ist zu lang.';
		}
		return '';
	}
	static function postcode ($input, $name) {
		if (!is_numeric($input) || $input < 0) {
			return 'Die Eingabe im Feld <i>%s</i> ist keine Postleitzahl.';
		}
		return '';
	}
	static function phone ($input, $name) {
		if ($input == '') {
			return '';
		}

		$input = str_replace(array('-','/','+',' '), array('','','',''), $input);

		if (!is_numeric($input)) {
			return 'Die Eingabe im Feld <i>%s</i> ist keine korrekte Telefonnummer.';
		}
		return '';
	}
	static function email ($input, $name) {
		if ($input == '') {
			return '';
		}

		if (!preg_match('|^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,6}$|',$input)) {
			return 'Die Eingabe im Feld <i>%s</i> ist keine korrekte E-Mail-Adresse.';
		}
		return '';
	}
	static function tee ($input, $name) {
		global $lss;

		if (!in_array($input, $lss['sizes'])) {
			return 'Sie haben im Feld <i>%s</i> keine korrekte Auswahl getroffen.';
		}
		return '';
	}
	static function sex ($input, $name) {
		if (!in_array($input, array('m','w'))) {
			return 'Sie haben im Feld <i>%s</i> keine korrekte Auswahl getroffen.';
		}
		return '';
	}
	static function number ($input, $name) {
		if (!is_numeric($input)) {
			return 'Sie haben im Feld <i>%s</i> keine Zahl angegeben.';
		}
		return '';
	}
	static function boolean ($input, $name) {
		switch($input) {
			case 'true':
			case 'false':
			case '0':
			case '1':
			case '':
				return '';
			break;

			default:
				return 'Ihre Eingabe im Bezug auf das Elternteil ist fehlerhaft.';
			break;
		}
		return '';
	}
	static function date ($input, $name) {
		if (!is_numeric($input)) {
			return 'Die Eingabe im Feld <i>%s</i> ist kein korrektes Datum.';
		} elseif ($name == 'bd_day' && ($input < 1 || $input > 31)) {
			return 'Die Eingabe im Feld <i>%s</i> ist kein korrektes Datum.';
		} elseif ($name == 'bd_month' && ($input < 1 || $input > 12)) {
			return 'Die Eingabe im Feld <i>%s</i> ist kein korrektes Datum.';
		} elseif ($name == 'bd_year' && ($input < date('Y')-100)) {
			return 'Mit diesem Alter sollten Sie sich persönlich erkundigen, ob die Strecke für Sie noch schaffbar ist.';
		} elseif ($name == 'bd_year' && $input > date('Y')) {
			return 'Sie sind noch nicht geboren.';
		}
		return '';
	}

	static function checked($name, $value, $form) {
		if ($form[$name] == $value)
			return 'checked="checked"';
		else
			return '';
	}


	static function error_info($errors, $fields, $show) {
		$current = array();
		if (!$show) {
			foreach ($errors as $field => $error) {
				if (is_numeric($field)) {
					$current[] = $error;
				}
			}
		} else {
			foreach ($errors as $field => $error) {
				if (in_array($field, $fields)) {
					$current[] = $error;
				}
			}
		}
		if (count($current) > 0) {
			$output = '';
			foreach ($current as $error) {
				$output .= '<li>'.$error.'</li>';
			}
			return '<div class="error"><ul>'.$output.'</ul></div>';
		}
		return '';
	}

	static function error_to_show($errors, $fields) {
		foreach ($errors as $field => $error) {
			if (in_array($field, $fields)) {
				return true;
			}
		}
		return false;
	}

	static function hidden_fields($form, $fields) {
		$output = '';
		foreach($form as $field=>$value) {
			if (!in_array($field, $fields)) {
				$output .= '<input type="hidden" name="'.$field.'" value="'.$value.'"/>';
			}
		}
		return $output;
	}

	static function input_text($data, $form, $field) {
		return
		'<tr><td>'.
			'<label for="'.$field.'">'.$data[$field][0].'</label>'.
		'</td><td>'.
			'<input type="text" value="'.$form[$field].'" name="'.$field.'" id="'.$field.'"/>'.
		'</td></tr>';
	}
	static function text($data, $form, $field) {
		return
		'<tr><td>'.$data[$field][0].'</td><td>'.
		(($form[$field] === '')?'Keine Angabe':$form[$field]).
		'</td></tr>';
	}
	static function route($input, $name) {
		global $lss;

		if ($input === '') return 'Bitte wählen Sie eine Strecke.';
    foreach ($lss['routes'] as $route) {
      if ($route['db'] == $input) return '';
    }
    if ($input == "doppelt") return '';
    return 'Bitte wählen Sie eine korrekte Strecke.';
	}

	static function information() {
		global $lss, $con;

		$moreOutput = '';
		foreach ($lss['routes'] as $key => $route) {
			$count = getRegistrationCount($key);
			if ($count >= $route['max']) {
				$moreOutput .= '<li>'.$route['name'].' - '.$route['max'].' Anmeldungen</li>';
			}
		}
		if ($moreOutput != '') {
			echo
				'<div class="error">',
					'<h3>Folgende Strecken haben die Teilnehmerbegrenzung erreicht:</h3>',
					'<ul>'.$moreOutput.'</ul>',
				'</div>';
		}

		return $con->text('information');
	}

	static function calculate_age($day, $month, $year) {
		global $lss;
		$age = date('Y',$lss['date']) - $year;
		if (date('m', $lss['date']) < $month) $age--;
		elseif (date('m',$lss['date']) == $month && date('d', $lss['date']) < $day) $age--;
		return $age;
	}

	static function getRegistrationCount($route) {
		global $lss;

		$result = mysql_query("
			SELECT COUNT(`id`) AS `count`
			FROM `".$lss['table']."`
			WHERE `route` = '".$lss['routes'][$route]['db']."'");

		$row = mysql_fetch_assoc($result);
		return $row['count'];
	}


	static function getRouteName($routedb) {
		global $config;

		if ($routedb == "doppelt") {
			return "Doppelstart";
		}

		foreach ($config['lss']['routes'] as $route) {
			if ($route['db'] == $routedb) {
				return $route['name'];
			}
		}
	}

	static function getMoney($routedb) {
		global $config;

		if ($routedb == "doppelt") {
			return 20;
		}

		foreach ($config['lss']['routes'] as $route) {
			if ($route['db'] == $routedb) {
				return $route['money'];
			}
		}
	}
}
