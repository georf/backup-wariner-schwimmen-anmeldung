<?php
class Page {
	public static function get($path, $asLink = false, $html = true) {
		global $config;

		if (!$asLink) {
			if ($config['log']['defaultTo'] === $path)
				return ($html)?'<em>'._('Homepage').'</em>':_('Homepage');
			else
				return ($html)?$path:wordwrap($path,25,'\n',1);
		} else {
			if ($config['log']['defaultTo'] === $path)
				return ($html)?
					WNode::getA(
					$config['log']['host'].
					$config['log']['removePath'].
					htmlspecialchars($path, ENT_COMPAT, 'UTF-8'),
					'<em>'._('Homepage').'</em>')
					:
					$config['log']['host'].
					$config['log']['removePath'].
					$path;
			else
				return ($html)?
					WNode::getA(
					$config['log']['host'].
					$config['log']['removePath'].
					htmlspecialchars($path, ENT_COMPAT, 'UTF-8'),
					htmlspecialchars(wordwrap($path,55,' ',1), ENT_COMPAT, 'UTF-8'))
					:
					$config['log']['host'].
					$config['log']['removePath'].
					$path;
		}
	}
}
