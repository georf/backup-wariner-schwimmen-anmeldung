<?php

class WForm_Radio extends WForm_Element {

	protected $elements;

	public function __construct($name, $description, $elements) {
		parent::__construct($name, $description);
		$this->elements = $elements;
	}

	public function getInput() {
		$output = '';

		for ($i = 0; $i < count($this->elements); $i++) {
			if ($i == 0) {
				$name = $this->getName();
			} else {
				$name = $this->getName().$i;
				$output .= new WNode('br', true);
			}

			$input = WNode::getInput($this->getName(), $this->elements[$i]->getName(), 'radio');
			$input->setId($name)->setClass($this->getClassNames());
			if ($this->value == $this->elements[$i]->getName()) {
				$input->setAttribute('checked', 'checked');
			}

			$output .= $input.
				WNode::getLabel($this->elements[$i]->getDescription(), $name);
		}

		return $output;
	}
}
