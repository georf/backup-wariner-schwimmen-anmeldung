<?php

class WForm_Date extends WForm_Element {

	const NOT_IS_NULL = 1;
	const NOT_IS_ZERO = 2;

	protected $options;

	public function __construct($name, $description, $options = array()) {
		parent::__construct($name, $description);

		if (!is_array($options)) {
			$options = array($options);
		}
		$this->options = $options;
	}

	public function getInput() {

		$set = true;

		if ((in_array(self::NOT_IS_NULL, $this->options) && $this->value == null)
		|| (in_array(self::NOT_IS_ZERO, $this->options) && $this->value == '0000-00-00')) {
			$set = false;
		}

		$day = WNode::getSelect($this->getName().'_day');
		if (!$set) {
			$day->setAttribute('disabled', 'disabled');
		} elseif ($this->value === null) {
			$day_set = 1;
		} else {
			$day_set = date('d', strtotime($this->value));
		}
		$day->setId($this->getName())->setClass($this->getClassNames());
		for ($i=1; $i<32; $i++) {
			if ($set && $day_set == $i)
				$day->appendChild(WNode::getOption($i, $i, true));
			else
				$day->appendChild(WNode::getOption($i, $i));
		}

		$month = WNode::getSelect($this->getName().'_month');
		if (!$set) {
			$month->setAttribute('disabled', 'disabled');
		} elseif ($this->value === null) {
			$month_set = 1;
		} else {
			$month_set = date('m', strtotime($this->value));
		}
		$month->setId($this->getName().'_month')->setClass($this->getClassNames());
		for ($i=1; $i<13; $i++) {
			if ($set && $month_set == $i)
				$month->appendChild(WNode::getOption($i, $i, true));
			else
				$month->appendChild(WNode::getOption($i, $i));
		}


		$year = WNode::getInput($this->getName().'_year', '', 'text');
		if (!$set) {
			$year->setAttribute('disabled', 'disabled');
		} elseif ($this->value === null) {
			$year->setValue('');
		} else {
			$year->setValue(date('Y', strtotime($this->value)));
		}
		$year->setId($this->getName().'_year')->setClass($this->getClassNames());

		$hidden = WNode::getInput($this->getName(), $this->value, 'hidden')->setId($this->getName().'_hidden');

		$js = new WNode('script');
		$js->setAttribute('type', 'text/javascript');
		$js->appendChild("
$(function(){
	$('#".$this->getName().",#".$this->getName()."_month,#".$this->getName()."_year').bind('change blur', function() {
		$('#".$this->getName()."_hidden').val($('#".$this->getName()."_year').val() + '-' + $('#".$this->getName()."_month').val() + '-' + $('#".$this->getName()."').val());
	});
});
");
		if (in_array(self::NOT_IS_NULL, $this->options) || in_array(self::NOT_IS_ZERO, $this->options)) {
			if (in_array(self::NOT_IS_NULL, $this->options)) {
				$default = 'NULL';
			} elseif (in_array(self::NOT_IS_ZERO, $this->options)) {
				$default = '0000-00-00';
			}
			$checkbox = WNode::getCheckbox($this->getName().'_set', '', !$set)->setId($this->getName().'_set');
			$js->appendChild("
$(function(){
	$('#".$this->getName()."_set').bind('change blur', function() {
		if(!$('#".$this->getName()."_set').attr('checked')) {
			$('#".$this->getName().",#".$this->getName()."_month,#".$this->getName()."_year').removeAttr('disabled');
			$('#".$this->getName()."_hidden').val($('#".$this->getName()."_year').val() + '-' + $('#".$this->getName()."_month').val() + '-' + $('#".$this->getName()."').val());
		} else {
			$('#".$this->getName().",#".$this->getName()."_month,#".$this->getName()."_year').attr('disabled', 'disabled');
			$('#".$this->getName()."_hidden').val('".$default."');
		}
	});
});
");
		} else {
			$checkbox = '';
		}

		return $day.$month.$year.$checkbox.$hidden.$js;
	}
}
