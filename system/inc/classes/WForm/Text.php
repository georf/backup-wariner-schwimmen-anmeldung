<?php

class WForm_Text extends WForm_Element {
	public function getInput() {
		return WNode::
			getInput($this->getName(), $this->value, 'text')
			->setId($this->getName())->setClass($this->getClassNames());
	}
}
