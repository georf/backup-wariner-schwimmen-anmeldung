<?php

abstract class WForm_Element {

	protected $name;
	protected $description;
	protected $value;
	protected $extraClasses;

	public function __construct($name, $description) {
		$this->name = $name;
		$this->description = $description;
	}

	public function getName() {
		return $this->name;
	}

	public function getDescription() {
		return $this->description;
	}

	public function getLabel() {
		return WNode::getLabel($this->getDescription(), $this->getName())->setClass($this->getClassNames());
	}

	public function getInput() {
		return '';
	}

	public function setValue($value) {
		$this->value = $value;
	}

	public function setExtraClasses($extraClasses) {
		$this->extraClasses = $extraClasses;
	}

	public function getClassNames() {
		return get_class($this).' '.get_class().' '.$this->extraClasses;
	}
}
