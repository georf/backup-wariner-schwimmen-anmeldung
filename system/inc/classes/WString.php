<?php
/**
 * Contains some helpfull functions for strings
 *
 * @author sg
 */
class WString {

	public static function abbreviate($text, $maxLength) {
		return self::abbreviateMiddle($text, $maxLength);
	}

	public static function abbreviateMiddle($text, $maxLength, $frontCharacters = 0.5) {
		$p = $text;
		if (strlen($p)<= $maxLength) {
			return $p;
		}
		$short = "";
		$short .= trim(substr($p, 0, floor($maxLength * $frontCharacters - 2)));
		$short .= "... ";
		$short .= trim(substr($p, ($maxLength - strlen($short))*(-1)));
		return $short;
	}
}
?>
