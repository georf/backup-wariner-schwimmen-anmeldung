<?php

class Registration {
  public static function get250() {
    global $db, $lss;

    return $db->getRows("
      SELECT *
      FROM `".$lss['table']."`
      WHERE route = '250'"
    );  
  }
  public static function get400() {
    global $db, $lss;

    return $db->getRows("
      SELECT *
      FROM `".$lss['table']."`
      WHERE route = '400'"
    ); 
  }
  public static function get1600() {
    global $db, $lss;

    return $db->getRows("
      SELECT *
      FROM `".$lss['table']."`
      WHERE route = '1600' OR route = 'doppelt'"
    ); 
  }
  public static function get4000() {
    global $db, $lss;

    return $db->getRows("
      SELECT *
      FROM `".$lss['table']."`
      WHERE route = '4000' OR route = 'doppelt'"
    ); 
  }
}