<?php

/**
 * Provides easy handling with (X)HTML elements
 *
 * Nodes are an easy way to handle HTML elements inspired by
 * Java Script. When you get a tag from the CMS you can
 * easily add and change some attributes or append and remove 
 * child nodes e.g. Thanks to the toSting functionality it can
 * also be used as a simple string.
 */
class WNode {

    /**
     * Defines the tag name of the node, e.g. img
     *
     * @var string
     */
    private $tagName;

    /**
     * Defines the element to be standalone
	 *
	 * Standalone means that the element does not have any child nodes. In
	 * XHTML these tags are marked like <img />.
     *
     * @var boolean
     */
    private $standalone;

    /**
     * Containing child nodes
     *
     * @var array (WNode, string)
     */
    private $childNodes;

    /**
     * The attributes of the node
     *
     * @var array
     */
    private $attributes;

	/**
	 *
	 * @param string $tagName Name of the element
	 * @param boolean $standalone Defines whether element is standalone or not
	 */
    public function __construct($tagName, $standalone = false) {
        $this->tagName = $tagName;
        $this->standalone = $standalone;
        $this->attributes = array();
        return $this;
    }

    /**
     * Sets the tag name for the element, e.g. img
     *
     * @param string $name
     */
    public function setTagName($name) {
        $this->tagName = $name;
        return $this;
    }

    /**
     * Appends an node or a string as child
     *
     * @param mixed $childNode
     */
    public function appendChild($childNode) {
        $this->childNodes[] = $childNode;
        return $this;
    }

    /**
     * Removes a child from the node
     *
     * Child nodes are numbered like an array starting with 0.
     *
     * @param int $childNumber
     */
    public function removeChild($childNumber) {
        unset($this->childNodes[$childNumber]);
        return $this;
    }

    /**
     * Returns an array of all child nodes
     *
     * @return array child nodes
     */
    public function getChildNodes() {
        return $this->childNodes;
    }

    public function getAttribute($attribute) {
        if (array_key_exists($attribute, $this->attributes)) {
            return $this->attributes[$attribute];
        }
        return null;
    }

    /**
     * Sets or overwrites an attribute of the node
     *
     * @param string $attribute Attributes name
     * @param string $value Attributes value
     */
    public function setAttribute($attribute, $value) {
        $this->attributes[$attribute] = $value;
        return $this;
    }

    /**
     * Defines this node as standalone
	 *
	 * @param boolean $set Set the standalone bit
     */
    public function setStandalone($set = true) {
        if ($set) {
			$this->standalone = true;
		} else {
			$this->standalone = false;
		}
		return $this;
    }

    private function getStandaloneString() {
        $o = "<".$this->tagName;
        foreach ($this->attributes as $att => $val) {
            $o .= " ".$att."=\"".$val."\"";
        }
        $o .= "/>";
        return $o;
    }

    private function getNormalString() {
        $o = "<".$this->tagName;
        foreach ($this->attributes as $att => $val) {
            $o .= " ".$att."=\"".$val."\"";
        }
        $o .= ">";
		if (isset($this->childNodes) && is_array($this->childNodes)) {
			foreach ($this->childNodes as $child) {
				$o .= $child;
			}
		}
        $o .= "</".$this->tagName.">";
        return $o;
    }

    public function __toString() {
        if ($this->standalone) {
            return $this->getStandaloneString();
        } else {
            return $this->getNormalString();
        }
    }

	/*
	 * == Handy Attributes =================================================
	 */
	 
	public function setName($name) {
		return $this->setAttribute("name", $name);
	}
	
	public function setId($value) {
		return $this->setAttribute("id", $value);
	}
	
	public function setStyle($value) {
		return $this->setAttribute("style", $value);
	}
	
	public function getStyle() {
		return $this->getAttribute("style");
	}
	
	public function setValue($value) {
		return $this->setAttribute("value", $value);
	}
	
	public function setClass($value) {
		return $this->setAttribute("class", $value);
	}

    public function getClass() {
		return $this->getAttribute("class");
	}
	
	public function setTitle($value) {
		return $this->setAttribute("title", $value);
	}

	/*
	 * == Static Nodes =================================================
	 */

	/**
	 * Returns an a node
	 *
	 * @param string $url
	 * @param WNode $text
	 */
    public static function getA($url, $text) {
		$n = new WNode("a");
		$n->setAttribute("href", $url);
		$n->appendChild($text);
		return $n;
	}

	/**
	 * Returns a p node
	 *
	 * @param WNode $child
	 * @return WNode node
	 */
	public static function getP($child = null) {
		$n = new WNode("p");
		if (isset($child)) {
			$n->appendChild($child);
		}
		return $n;
	}

	/**
	 * Returns h1 node
	 *
	 * @param WNode $child
	 * @return WNode
	 */
	public static function getH1($child = null) {
		$n = new WNode("h1");
		if (isset($child)) {
			$n->appendChild($child);
		}
		return $n;
	}

	/**
	 * Returns h2 node
	 *
	 * @param WNode $child
	 * @return WNode
	 */
	public static function getH2($child = null) {
		$n = new WNode("h2");
		if (isset($child)) {
			$n->appendChild($child);
		}
		return $n;
	}

	/**
	 * Returns h3 node
	 *
	 * @param WNode $child
	 * @return WNode
	 */
	public static function getH3($child = null) {
		$n = new WNode("h3");
		if (isset($child)) {
			$n->appendChild($child);
		}
		return $n;
	}

	/**
	 * Returns h4 node
	 *
	 * @param WNode $child
	 * @return WNode
	 */
	public static function getH4($child = null) {
		$n = new WNode("h4");
		if (isset($child)) {
			$n->appendChild($child);
		}
		return $n;
	}

	public static function getImg($src, $alt = "", $title = null) {
		$n = new WNode("img", true);
		$n->setAttribute("src", $src);
		$n->setAttribute("alt", $alt);
		if (isset($title)) {
			$n->setAttribute("title", $title);
		}
		return $n;
	}
	
	public static function getDiv($children = null) {
		$n = new WNode("div");
		if (isset($children)) {
			$n->appendChild($children);
		}
		return $n;
	}
	
	public static function getSpan($children = null) {
		$n = new WNode("span");
		if (isset($children)) {
			$n->appendChild($children);
		}
		return $n;
	}

	public static function getHr() {
		return new WNode("hr", true);
	}

    public static function getInput($name, $value, $type) {
        $t = new WNode("input", true);
        $t->setAttribute("name", $name);
        $t->setAttribute("value", $value);
        $t->setAttribute("type", $type);
        return $t;
    }

    public static function getTextarea($name, $value) {
        $t = new WNode("textarea");
        $t->setAttribute("name", $name);
        $t->appendChild($value);
        return $t;
    }

	/**
	 * Returns a select node
	 *
	 * @param <type> $name
	 * @return <type> WNode
	 */
    public static function getSelect($name) {
        $t = new WNode("select");
        $t->setAttribute("name", $name);
        return $t;
    }

	/*
    public static function getOption($name, $value, $text, $selected = false) {
        $t = new WNode("option", "block");
        $t->setAttribute("name", $name);
        if ($selected) {
            $t->setAttribute("selected", "selected");
        }
        $t->setAttribute("value", $value);
        $t->appendChild($text);
        return $t;
    }*/

	public static function getOption($value, $text, $selected = false) {
        $t = new WNode("option");
        if ($selected) {
            $t->setAttribute("selected", "selected");
        }
        $t->setAttribute("value", $value);
        $t->appendChild($text);
        return $t;
    }

    public static function getButton($name, $text, $type, $value = null) {
        $t = new WNode("button");
        $t->setAttribute("name", $name);
        $t->appendChild($text);
		$t->setAttribute("type", $type);
        if (isset($value)) {
            $t->setAttribute("value", $value);
        }
        return $t;
    }
	
	public static function getCheckbox($name, $value, $checked = false) {
		$n = new WNode("input", true);
		$n->setAttribute("name", $name);
		$n->setAttribute("value", $value);
		$n->setAttribute("type", "checkbox");
		if ($checked) {
			$n->setAttribute("checked", "checked");
		}
		return $n;
	}
	
	public static function getLabel($text, $for = null) {
		$n = new WNode("label");
		if (isset($for)) {
			$n->setAttribute("for", $for);
		}
		$n->appendChild($text);
		return $n;
	}
}
?>
