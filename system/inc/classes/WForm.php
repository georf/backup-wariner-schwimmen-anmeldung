<?php

class WForm extends WNode {

	protected $data;

	public function __construct($data, $action = '', $method = 'post') {
		parent::__construct('form');

		$this->setAttribute('action', $action);
		$this->setAttribute('method', $method);

		$this->data = $data;
	}

	public function addGroup($name, $fields, $classes = '') {
		$fieldset = self::getGroup($name, $fields, $this->data, $classes);
		$this->appendChild($fieldset);
		return $this;
	}

	public static function getGroup($name, $fields, $data, $classes = '') {
		$fieldset = new WNode('fieldset');

		if ($name !== null) {
			$legend = new WNode('legend');
			$legend->appendChild($name);
			$fieldset->appendChild($legend);
		}

		$table = new WTable();
		$table->addBody();

		foreach ($fields as $field) {
			if (isset($data[$field->getName()])) {
				$field->setValue($data[$field->getName()]);
			}

			$field->setExtraClasses($classes);

			$table->addTr();
			$table->addTd($field->getLabel());
			$table->addTd($field->getInput());
		}

		$fieldset->appendChild($table);
		return $fieldset;
	}
}
