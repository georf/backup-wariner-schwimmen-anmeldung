<?php

class Date {
	public static function calculateAge($day, $month, $year) {
		global $lss;
		$age = date('Y',$lss['date']) - $year;
		if (date('m', $lss['date']) < $month) $age--;
		elseif (date('m',$lss['date']) == $month && date('d', $lss['date']) < $day) $age--;
		return $age;
	}
}
