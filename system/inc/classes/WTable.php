<?php

/**
 * Provides easy handling with WNode tables
 *
 */
class WTable extends WNode {

	var $currentBody;
	var $currentTr;
	var $currentTd;
	var $even = false;

    public function __construct() {
		parent::__construct("table");
		return $this;
	}
	
	public static function get() {
		$t = new WTable();
		return $t;
	}

	public function addBody() {
		if (isset($this->currentBody)) {
			if (isset($this->currentTr)) {
				if (isset($this->currentTd)) {
					$this->currentTr->appendChild($this->currentTd);
					unset($this->currentTd);
				}
				$this->currentBody->appendChild($this->currentTr);
				unset($this->currentTr);
			}
			$this->appendChild($this->currentBody);
		}
		$this->currentBody = new WNode("tbody");
		return $this;
	}
	
	public function addHead() {
		if (isset($this->currentBody)) {
			if (isset($this->currentTr)) {
				if (isset($this->currentTd)) {
					$this->currentTr->appendChild($this->currentTd);
					unset($this->currentTd);
				}
				$this->currentBody->appendChild($this->currentTr);
				unset($this->currentTr);
			}
			$this->appendChild($this->currentBody);
		}
		$this->currentBody = new WNode("thead");
		return $this;
	}

	/**
	 * Adds a TR to the table
	 * 
	 * Use parameter to force TR to be even (=== false) or odd 
	 * (=== true). Leave empty/null for calculated default value.
	 * 
	 * @param boolean $isOdd
	 */
	public function addTr($isOdd = null) {
		if (!isset($this->currentBody)) {
			$this->addBody();
		}
		if (isset($this->currentTr)) {
			if (isset($this->currentTd)) {
				$this->currentTr->appendChild($this->currentTd);
				unset($this->currentTd);
			}
			$this->currentBody->appendChild($this->currentTr);
		}
		if ($isOdd === true) $this->even = false;
		else if ($isOdd === false) $this->even = true;
		$this->currentTr = new WNode("tr");
		if ($this->even) $this->currentTr->setClass("even");
		else $this->currentTr->setClass("odd");
		$this->even = !$this->even;
		return $this;
	}

	public function addTd($content, $attributes = array()) {
		if (!isset($this->currentTr)) {
			$this->addTr();
		}
		if (isset($this->currentTd)) {
			$this->currentTr->appendChild($this->currentTd);
		}
		$this->currentTd = new WNode("td");
		$this->currentTd->appendChild($content);
		foreach ($attributes as $key => $value) {
			$this->currentTd->setAttribute($key, $value);
		}
		return $this;
	}

	public function addTh($content, $attributes = array()) {
		if (!isset($this->currentTr)) {
			$this->addTr();
		}
		if (isset($this->currentTd)) {
			$this->currentTr->appendChild($this->currentTd);
		}
		$this->currentTd = new WNode("th");
		$this->currentTd->appendChild($content);
		foreach ($attributes as $key => $value) {
			$this->currentTd->setAttribute($key, $value);
		}
		return $this;
	}

	public function __toString() {
		if (isset($this->currentTd)) {
			//if (!isset($this->currentTr)) $this->addTr();
			$this->currentTr->appendChild($this->currentTd);
			unset($this->currentTd);
		}
		if (isset($this->currentTr)) {
			//if (!isset($this->currentBody)) $this->addBody();
			$this->currentBody->appendChild($this->currentTr);
			unset($this->currentTr);
		}
		if (isset($this->currentBody)) {
			$this->appendChild($this->currentBody);
			unset($this->currentBody);
			return parent::__toString();
		}
		return "";
	}

	/**
	 * Returns a reference to the last inserted TD
	 *
	 * @return WNode Latest TD
	 */
	public function &getTd() {
		return $this->currentTd;
	}
	
	/**
	 * Returns a reference to the last inserted TH
	 *
	 * @return WNode Latest TH
	 */
	public function &getTh() {
		return $this->currentTd;
	}

	/**
	 * Returns a reference to the last inserted TR
	 *
	 * @return WNode Latest TR
	 */
	public function &getTr() {
		return $this->currentTr;
	}

	/**
	 * Returns a reference to the last inserted TBODY
	 *
	 * @return WNode Latest TBODY
	 */
	public function &getBody() {
		return $this->currentBody;
	}
	
	/**
	 * Resets TR's even state (i.e. to 1 == odd)
	 */
	public function resetTrState() {
		$this->even = false;
		return $this;
	}
}
?>
