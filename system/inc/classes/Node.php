<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of node
 *
 * @author sg
 */
class Node extends WNode {
	public static function getFieldset($name = null, $description = null, $fields = null) {
		$fieldset = new WNode("fieldset");

        if ($name !== null) {
            $legend = new WNode("legend");
            $legend->appendChild($name);
            $fieldset->appendChild($legend);
        }

		if (isset($description)) {
			$p = new WNode("p");
			$p->setAttribute("class", "description");
			$p->appendChild($description);
			$fieldset->appendChild($p);
		}

		if (isset($fields)) {
			$fieldset->appendChild($fields);
		}

		return $fieldset;
	}

	public static function getFormTable() {
		$table = new WNode("table");
		$table->setClass("formTable");
		return $table;
	}

	public static function getFormTr($description, $field) {
		$tr = new WNode("tr");
		$td = new WNode("td");
		$td->setAttribute("class", "description");
		$td->appendChild($description);
		$tr->appendChild($td);
		$td = new WNode("td");
		$td->appendChild($field);
		$tr->appendChild($td);
		return $tr;
	}

	public static function getImportantButtonDiv($button) {
		$div = new WNode("div");
		$div->setAttribute("class", "importantButton");
		$span = new WNode("span");
		$span->appendChild($button);
		$div->appendChild($span);
		return $div;
	}

	public static function getIcon($icon, $title = null) {
		if (strpos($icon, "/") === false) {
			$icon = "./templates/cms/images/16/".$icon;
		}
		$img = WNode::getImg($icon, "", null);
		$img->setAttribute("class", "icon");
		return $img;
	}

	public static function getSimpleMenu() {
		$me = new Node("ul");
		$me->setClass("simpleMenu");
		return $me;
	}

	public static function getSimpleMenuItem($text, $link, $icon = null) {
		$i = new Node("div");
		if ($icon == null) {
			$i->appendChild(Node::getIconA($link, "find.png", $text));
		} else {
			$i->appendChild(Node::getIconA($link, $icon, $text));
		}
		$li = new Node("li");
		$li->appendChild($i);
		return $li;
	}

	public static function getIconSpan($icon, $text) {
		if (strpos($icon, "/") === false) {
			$icon = "templates/images/16/".$icon;
		}
		$s = new WNode("span");
		$s->setClass("icon");
		$s->setStyle("background-image:url(".$icon.");");
		$s->appendChild($text);
		return $s;
	}

	public static function getIconA($href, $icon, $text) {
		if (strpos($icon, "/") === false) {
			$icon = "templates/images/16/".$icon;
		}
		$s = new WNode("a");
		$s->setClass("icon");
		$s->setStyle("background-image:url(".$icon.");");
		$s->setAttribute("href", $href);
		$s->appendChild($text);
		return $s;
	}
}
