$(function() {
  var jumpToPage = function (page, seconds) {
      var rest = 10,
        restPointer = [],
        i = 0, z = 0;

      $('#nextFooter').find('h3').text(page[1]);

      for (; rest > 0; rest--) {
        restPointer.push(rest);
      }

      for (; i < restPointer.length; i++) {
        setTimeout(function () {
          $('#nextFooter').slideDown();
          $('#seconds').text(restPointer[z]);
          z++;
        }, (seconds - restPointer[i]) * 1000);
      }

      setTimeout(function () {
        location.search = page[0];
      }, seconds*1000);

    },
    pages = [
      ['?page=ausschreibung', 'Ausschreibung', 30],
      ['?page=route',      'Alle Anmeldungen'],
      ['?page=route&route=250', 'Anmeldungen für 250 m'],
      ['?page=route&route=400', 'Anmeldungen für 400 m'],
      ['?page=route&route=1600', 'Anmeldungen für 1600 m'],
      ['?page=route&route=4000', 'Anmeldungen für 4000 m']
    ],
    search = location.search,
    current = 0,
    next = 0,
    i = 0, z = 0,
    seconds,
    lines = [];

  for (i = 0; i < pages.length; i++) {
    if (pages[i][0] == search) {
      current = i;
      break;
    }
  }

  next = current + 1;
  if (!pages[next]) next = 0;

  if (!pages[current][2]) {
    $('tr').each(function() {
      lines.push($(this));
    });

    for (i = 0; i < lines.length; i++) {
      setTimeout(function() {
        $('html,body').animate({
          scrollTop: lines[z].offset().top
        }, 1000);
        z++;
      }, 1001 * i);
    };

    seconds = Math.round(999 * lines.length / 1000);

    if (seconds < 11) seconds = 11;
  } else {
    setTimeout(function() {
      $('html,body').animate({
            scrollTop: $('#endOfPage').offset().top
      }, pages[current][2] * 1000);
    }, 3000);
    seconds = pages[current][2];
  }

  jumpToPage(pages[next], seconds);

});
