<?php

$insert = true;
$form = array();

foreach ($config['table-fields'] as $field) {
	if (isset($_POST[$field])) {
		$form[$field] = $_POST[$field];
	}
}

if (count($form) > 0) {
	if ($form['age'] > 17) {
		$form['parent'] = '0';
		$form['parent_name'] = '';
		$form['parent_firstname'] = '';
	} else {
		$form['parent'] = '1';
	}
	$result = $db->insertRow($lss['table'], $form);
	if ($result !== false) {
		// success -> link to edit user
		header('Location: ?page=swimmer&id='.$result);
		exit();
	}
}


?>


<div class="sixteen columns">
	<h1>Neuer Schwimmer</h1>
</div>
<br class="clear">
<?php



$sizes = array();
foreach ($lss['sizes'] as $size) {
	$sizes[] = new WForm_Option($size, $size);
}
$routes = array();
foreach ($lss['routes'] as $route) {
  $routes[] = new WForm_Option($route['db'], $route['name']);
}
$routes[] = new WForm_Option("doppelt", "Doppelstart");

$f = new WNode('form');
$f
	->setAttribute('method', 'post')
	->setId('swimmer-new-form')
	->appendChild(WNode::getDiv()
		->setClass('ten columns')
		->appendChild(
			WForm::getGroup('Pflichtdaten', array(
				new WForm_Text('firstname', 'Vorname'),
				new WForm_Text('name', 'Name'),
				new WForm_Radio('sex', 'Geschlecht', array(
					new WForm_Option('m', 'Männlich'),
					new WForm_Option('w', 'Weiblich'),
				)),
				new WForm_Text('age', 'Alter'),
				new WForm_Text('street', 'Straße und Nr.'),
				new WForm_Text('city', 'Stadt'),
				new WForm_Radio('size', 'T-Shirt-Größe', $sizes),
			), $form, 'required')
		)
		->appendChild(WNode::getButton('edit', 'Weiter', 'submit')->setId('next'))
	)
	->appendChild(WNode::getDiv()
		->setClass('six columns')
		->appendChild(
			WForm::getGroup('Eltern', array(
				new WForm_Text('parent_firstname', 'Vorname'),
				new WForm_Text('parent_name', 'Name'),
			), $form, 'required')
			->setStyle('display:none;')
			->setId('parent_fieldset')
		)
		->appendChild(
			WForm::getGroup('Schwimmen', array(
				new WForm_Radio('route', 'Strecke', $routes),
				new WForm_Text('ak', 'AK'),
			), $form, 'required')
		)
	);
echo $f;
?>

<script type="text/javascript">
var checkForm = function() {

  var valid = true,

  age = $('#age').val().replace(/[^\d]+/g, '');

  $('#age').val(age);

  if (age < 18) {
    $('#parent_fieldset').slideDown();
  } else {
    $('#parent_fieldset').slideUp();
  }

  if (age < 11) {
    $('#route').removeAttr('disabled');
    $('#route').attr('checked','checked');

    $('#route1,#route2,#route3,#route4').attr('disabled', 'disabled');
    if ($('#route1').attr('checked')) {
      $('#route1').removeAttr('checked');
    }
    if ($('#route2').attr('checked')) {
      $('#route2').removeAttr('checked');
    }
    if ($('#route3').attr('checked')) {
      $('#route3').removeAttr('checked');
    }
    if ($('#route4').attr('checked')) {
      $('#route4').removeAttr('checked');
    }
    $('#ak').val('1');
  } else if (age < 14) {
    $('#route1').removeAttr('disabled');
    $('#route1').attr('checked','checked');

    $('#route,#route2,#route3,#route4').attr('disabled', 'disabled');
    if ($('#route').attr('checked')) {
      $('#route').removeAttr('checked');
    }
    if ($('#route2').attr('checked')) {
      $('#route2').removeAttr('checked');
    }
    if ($('#route3').attr('checked')) {
      $('#route3').removeAttr('checked');
    }
    if ($('#route4').attr('checked')) {
      $('#route4').removeAttr('checked');
    }

    $('#ak').val('2');
  } else {
    $('#route2,#route3,#route4').removeAttr('disabled');

    $('#route,#route1').attr('disabled', 'disabled');
    if ($('#route').attr('checked')) {
      $('#route').removeAttr('checked');
    }
    if ($('#route1').attr('checked')) {
      $('#route1').removeAttr('checked');
    }
    if (age < 28) {
      $('#ak').val('3');
    } else if (age < 46) {
      $('#ak').val('4');
    } else {
      $('#ak').val('5');
    }
  }

  if ($('#swimmer-new-form').valid() && valid) {
    $('#next').removeAttr('disabled');
    return true;
  } else {
    $('#next').attr('disabled', 'disabled');
    return false;
  }
}

$(function() {
	$('#firstname').focus();
	$('#ak').attr('disabled', 'disabled');
	$('#swimmer-new-form').validate();
	$("#swimmer-new-form :input").bind('paste keyup blur click', function() {
    checkForm();
  });
	$("#swimmer-new-form").bind('submit', function() {
    if (checkForm()) {
      $('input:disabled').removeAttr('disabled');
      return true;
    } else {
      return false;
    }
  });

	checkForm();
});

</script>
