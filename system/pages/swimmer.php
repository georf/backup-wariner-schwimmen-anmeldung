<?php

if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
	header('Location: ?page=swimmer_select');
	exit();
}

if (isset($_POST['paid'])) {
	$db->updateRow($lss['table'], $_GET['id'], array('paid'=>date('Y-m-d')));
  header('Location: ?page=swimmer&id='.$_GET['id']);
  exit();
}

if (isset($_POST['notpaid'])) {
	$db->updateRow($lss['table'], $_GET['id'], array('paid'=>'0000-00-00'));
  header('Location: ?page=swimmer&id='.$_GET['id']);
  exit();
}


if (isset($_POST['key'])) {
	$db->updateRow($lss['table'], $_GET['id'], array('key'=>$_POST['key']));
  header('Location: ?page=swimmer&id='.$_GET['id']);
  exit();
}


if (isset($_POST['sign'])) {
	$db->updateRow($lss['table'], $_GET['id'], array('signed'=>'1'));
  header('Location: ?page=swimmer&id='.$_GET['id']);
  exit();
}

if (isset($_POST['deliveried'])) {
	$db->updateRow($lss['table'], $_GET['id'], array('deliveried'=>date('Y-m-d H:i:s')));
  header('Location: ?page=swimmer_check&id='.$_GET['id']);
  exit();
}

if (isset($_POST['notdeliveried'])) {
	$db->updateRow($lss['table'], $_GET['id'], array('deliveried'=>NULL));
  header('Location: ?page=swimmer&id='.$_GET['id']);
  exit();
}

$result = $db->getFirstRow("
	SELECT *
	FROM `".$lss['table']."`
	WHERE `id` = '".$db->escape($_GET['id'])."'
	LIMIT 1;");

if ($result === false) {
	echo '<h2>Schwimmer nicht gefunden</h2>';
} else {

  $ready = true;

	$money = Test::getMoney($result['route']);

	if ($result['key'] != null) {
		echo '<h2>Schlüssel: <a id="key" href="">',$result['key'],'</a></h2>';
	}


	echo '<div class="seven columns">';

	echo '<h1>' . $result['firstname'] . ' ' . $result['name'] . '</h1>';

	echo
	'<table class="table">',
		Test::text($data, $result, 'firstname'),
		Test::text($data, $result, 'name'),
		'<tr><td>Geschlecht</td><td>',(($result['sex'] == 'm')?'Männlich':'Weiblich'),'</td></tr>',
		'<tr><td>Alter:</td><td>',$result['age'],' Jahre</td></tr>',
		Test::text($data, $result, 'city'),
		'<tr><td>Strecke</td><td>',Test::getRouteName($result['route']),'</td></tr>',
		'<tr><td>AK</td><td>',$result['ak'],'</td></tr>';
	if ($result['age'] < 18) {
		echo
		'<tr><td colspan="2">Erziehnungsberechtigter:</td></tr>',
		Test::text($data, $result, 'parent_firstname'),
		Test::text($data, $result, 'parent_name');
	}

	echo
		'</table>';
	echo '<div><a class="button" href="?page=swimmer_edit&amp;id=',$_GET['id'],'">Daten ändern</a></div>';

	echo '</div>';
	echo '<div class="four columns">',
    '<table class="table" style="margin-top:50px;">',
		'<tr><td>T-Shirt:</td><td style="font-weight:bold;">',$result['size'],'</td></tr>',
		'<tr id="paidrow"';
  if ($result['paid'] != '0000-00-00') {
    echo ' class="success clickable"';
  } else {

    echo ' class="fail"';
  }
  echo '><td>Gebühr:</td><td style="font-weight:bold;">',$money,' €</td></tr>';

    if ($result['deliveried'] != null) {
      $ready = false;
      echo
		'<tr id="deliveriedrow" class="success clickable"><td>Abgefertigt:</td><td style="font-weight:bold;">',date('d.m.Y H:i', strtotime($result['deliveried'])),'</td></tr>';
  }
  echo
    '</table>',
	'</div>';

	echo '<div class="five columns">';

	if ($result['key'] == null) {
    $ready = false;
		echo
			'<div class="rightBox fail">',
				'<form method="post">',
					'<input type="text" value="" name="key"/><br/>',
					'<button type="submit">Setze Schlüssel</button>',
				'</form>',
			'</div>';
	} else {
		echo
			'<div class="rightBox success" style="display:none;" id="keybox">',
				'<form method="post">',
					'<input type="text" value="',$result['key'],'" name="key"/><br/>',
					'<button type="submit">Setze Schlüssel</button>',
					'<button type="submit" id="keybox-cancel">Abbrechen</button>',
				'</form>',
			'</div>';
  }


	if ($result['paid'] == '0000-00-00') {
    $ready = false;
		echo
			'<div class="rightBox fail">',
				'<p>NICHT bezahlt<br/>',$money,' EUR</p>',
				'<form method="post">',
					'<input type="hidden" value="',$_GET['id'],'" name="paid"/>',
					'<button type="submit">Bezahlt</button>',
				'</form>',
			'</div>';
	}

  if ($result['signed'] == '0') {
    $ready = false;
		echo
			'<div class="rightBox fail">',
      '<p>Unterschrift';
    if ($result['age'] < 18) echo ' (Eltern)';
    echo '</p>',
				'<form method="post">',
					'<input type="hidden" value="',$_GET['id'],'" name="sign"/>',
					'<button type="submit">Unterschrieben</button>',
				'</form>',
			'</div>';
	}

  if ($ready) {
    echo
			'<div class="rightBox fail">',
				'<p>Fertigstellen</p>',
				'<form method="post">',
					'<input type="hidden" value="',$_GET['id'],'" name="deliveried"/>',
					'<button type="submit">Kleidung und Schild aushändigen</button>',
				'</form>',
			'</div>';
    }

	echo '</div>';
  ?>
<script type="text/javascript">
$(function() {
  $('#key').click(function() {
    $('#keybox').slideDown();
    return false;
  });
  $('#keybox-cancel').click(function() {
    $('#keybox').slideUp();
    $('input[name=key]').val($('#key').text());
    return false;
  });


  // test for success
  if (window.location.search.match(/success=1/)) {
    var box = $('<div class="success rightBox centerbox"><p>Aktion erfolgreich</p></div>');
    $('body').append(box);

    setTimeout(function() {
      box.fadeOut();
    }, 3000);
  }

  $('#paidrow.success').click(function() {
    var form = $('<form method="post"><input type="hidden" value="1" name="notpaid"/></form>');
    $('body').append(form);
    form.submit();
  });

  $('#deliveriedrow.success').click(function() {
    var form = $('<form method="post"><input type="hidden" value="1" name="notdeliveried"/></form>');
    $('body').append(form);
    form.submit();
  });

});

</script>
<?php
}
