<h1 id="all-swimmers-heading">Schwimmer auswählen</h1>

<table id="all-schwimmers">
	<thead>
	<tr><th>Nachname</th><th>Vorname</th><th>Alter</th><th>B</th><th>D</th></tr>
	</thead>
<?php

$registrations = $db->getRows("
	SELECT *
	FROM `".$lss['table']."`;
");

foreach ($registrations as $registration) {
	echo
	'<tr class="clickable" onclick="window.location=\'?page=swimmer&amp;id=',$registration['id'],'\'">',
		'<td>',$registration['name'],'</td>',
		'<td>',$registration['firstname'],'</td>',
		'<td>',$registration['age'],'</td>',
		'<td class="',(($registration['paid'] == '0000-00-00')?'notpaid':'paid'),'" title="',(($registration['paid'] == '0000-00-00')?'Nicht bezahlt':'Bezahlt'),'">&nbsp;</td>',
		'<td class="',(($registration['deliveried'] == null)?'notdeliveried':'deliveried'),'" title="',(($registration['deliveried'] == null)?'Nicht ausgehändigt':'Ausgehändigt'),'">&nbsp;</td>',
	'</tr>';
}
?>

</table>

<script type="text/javascript">
$(document).ready(function() {
    $('#all-schwimmers').dataTable({
		"sDom":'<"H"f>t<"F">',
		"iDisplayLength": -1,
		"aaSorting":[[0,'asc'],[1,'asc']],
		"oLanguage": {
			"sSearch": "Durchsuchen:"
		},
		"fnInitComplete": function () {
			$('div.dataTables_filter input').focus();
		}
	});
});
</script>
