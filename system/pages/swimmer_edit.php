<?php

if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
	header('Location: ?page=swimmer_select');
	exit();
}


$insert = true;
$form = array();
$errors = array();

$swimmer = $db->getFirstRow("
	SELECT *
	FROM `".$lss['table']."`
	WHERE `id` ='".$db->escape($_GET['id'])."'
	LIMIT 1;");

if ($swimmer === false) {
	header('Location: ?page=swimmer_select');
	exit();
}

$update = array();
foreach ($data as $check=>$extra) {

	if (isset($swimmer[$check])) {
		$form[$check] = $swimmer[$check];
	} else {
    $form[$check] = NULL;
  }

	$name		= $extra[0];
	$callback	= $extra[1];
	$required	= $extra[2];


	if (isset($_POST[$check]) && $_POST[$check] != $form[$check]) {
		$form[$check] = $_POST[$check];
		$update[$check] = $_POST[$check];

		if ($callback != '') {
			$error = call_user_func($callback, $_POST[$check], $check);
			if ($error != '' && $required) {
				$insert = false;
				$errors[] = sprintf($error, $name);
			}
		}
	}

	if (!isset($form[$check])) {
		$form[$check] = '';
	}
}

if (count($update) > 0 && count($errors) === 0) {
	if ($db->updateRow($lss['table'], $_GET['id'], $update)) {
		header('Location: ?page=swimmer&success=1&id='.$_GET['id']);
		exit();
	} else {
		echo '<h2>Fehler beim Speichern</h2>';
	}

	$age = Date::calculateAge($form['bd_day'], $form['bd_month'], $form['bd_year']);

	if ($age < 11)		$ak = 1;
	elseif ($age < 14)	$ak = 2;
	elseif ($age < 28)	$ak = 3;
	elseif ($age < 46)	$ak = 4;
	else $ak = 5;

	if ($ak < 3)	$money = $lss['small_money'];
	else			$money = $lss['big_money'];

	if ($age > 17) {
		$form['parent'] = '0';
		$form['parent_name'] = '';
		$form['parent_firstname'] = '';
	} else {
		$form['parent'] = '1';
	}

	$form['birthday'] = date('Y-m-d', mktime(0,0,0,$form['bd_month'],$form['bd_day'],$form['bd_year']));
	$form['size'] = $form['tee_size'];
	$form['route'] = $lss['routes'][$form['route']]['db'];
	$form['age'] = $age;
	$form['ak'] = $ak;
	unset($form['bd_month'], $form['bd_day'], $form['bd_year'], $form['tee_size']);
	$result = $db->insertRow($lss['table'], $form);
	if ($result !== false) {
		// success -> link to edit user
		header('Location: ?page=swimmer&id='.$result);
		exit();
	}
}

if (count($errors) > 0) {
	echo '<ul>';
	foreach ($errors as $error) {
		echo '<li>'.$error.'</li>';
	}
	echo '</ul>';
}

?>

<h1>Bearbeite</h1>

<?php

$sizes = array();
foreach ($lss['sizes'] as $size) {
	$sizes[] = new WForm_Option($size, $size);
}
$routes = array();
foreach ($lss['routes'] as $route) {
	$routes[] = new WForm_Option($route['db'], $route['name']);
}
$routes[] = new WForm_Option("doppelt", "Doppelstart");

$f = new WForm($swimmer);
echo $f->addGroup('Persönlich', array(
	new WForm_Text('firstname', 'Vorname'),
	new WForm_Text('name', 'Name'),
	new WForm_Radio('sex', 'Geschlecht', array(
		new WForm_Option('w', 'Weiblich'),
		new WForm_Option('m', 'Männlich'),
	)),
	new WForm_Text('age', 'Alter'),
))
->appendChild(WNode::getButton('edit', 'Speichern', 'submit'))
->addGroup('Eltern', array(
	new WForm_Radio('parent', 'Eltern', array(
		new WForm_Option('0', 'Nein'),
		new WForm_Option('1', 'Ja'),
	)),
	new WForm_Text('parent_firstname', 'Vorname'),
	new WForm_Text('parent_name', 'Name'),
))
->appendChild(WNode::getButton('edit', 'Speichern', 'submit'))
->addGroup('Adresse', array(
	new WForm_Text('street', 'Straße'),
	new WForm_Text('city', 'Stadt'),
))
->appendChild(WNode::getButton('edit', 'Speichern', 'submit'))
->addGroup('Schwimmen', array(
	new WForm_Radio('size', 'T-Shirt-Größe', $sizes),
	new WForm_Radio('route', 'Strecke', $routes),
	new WForm_Text('ak', 'AK'),
	new WForm_Text('key', 'Schlüssel'),
))->appendChild(WNode::getButton('edit', 'Speichern', 'submit'));
