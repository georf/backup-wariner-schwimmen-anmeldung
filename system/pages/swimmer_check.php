<?php

if (!isset($_GET['id']) || !is_numeric($_GET['id'])) {
	header('Location: ?page=swimmer_select');
	exit();
}

$result = $db->getFirstRow("
	SELECT *
	FROM `".$lss['table']."`
	WHERE `id` = '".$db->escape($_GET['id'])."'
	LIMIT 1;");

if ($result === false) {
	echo '<h2>Schwimmer nicht gefunden</h2>';
} else {
  echo '<h1>Bitte nochmals prüfen:</h1>';
  echo '<h1>Laut nachfragen!</h1>';

  echo '<table style="font-size:2em;line-height:1.1em">';
  echo '<tr><th>Schlüssel</th><td>'.$result["key"].'</td></tr>';
  echo '<tr><th>Alter</th><td>'.$result["age"].'</td></tr>';
  echo '<tr><th>Strecke</th><td>'.$result["route"].'</td></tr>';
  echo '<tr><th>Geschlecht</th><td>'.$result["sex"].'</td></tr>';
  echo '</table>';

  echo '<a class="button" href="?page=swimmer&id='.$result['id'].'">Zurück</a> <a class="button" href="?">OK</a>';
}
