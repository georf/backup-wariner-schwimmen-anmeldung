<?php

if (isset($_POST['export'])) {
  $registrations = array();
  $filename = 'anmeldungen';

  if (isset($_POST['250'])) {
    $registrations = $db->getRows("
      SELECT *
      FROM ".$lss['table']."
      WHERE `route`='250';
    ");
    $filename .= '-250';
  } elseif (isset($_POST['400'])) {
    $registrations = $db->getRows("
      SELECT *
      FROM ".$lss['table']."
      WHERE `route`='400';
    ");
    $filename .= '-400';
  } elseif (isset($_POST['1600'])) {
    $registrations = $db->getRows("
      SELECT *
      FROM ".$lss['table']."
      WHERE `route`='1600' or `route` = 'doppelt';
    ");
    $filename .= '-1600';
  } elseif (isset($_POST['4000'])) {
    $registrations = $db->getRows("
      SELECT *
      FROM ".$lss['table']."
      WHERE `route`='4000' or `route` = 'doppelt';
    ");
    $filename .= '-4000';
  } elseif (isset($_POST['all'])) {
    $registrations = $db->getRows("
      SELECT *
      FROM ".$lss['table'].";
    ");
  }

  
  // Creating a workbook
  $objPHPExcel = new PHPExcel();
  $sheet = $objPHPExcel->setActiveSheetIndex(0);

  function col($l, $i) {
    return chr(65+$i).$l;
  }

  $l = 1;
  //Tabellenüberschrift erzeugen
  $head = array(
    'Nummer',
    'AK',
    'Strecke',
    'Name',
    'Vorname',
    'Alter',
    'Geschlecht',
    'Wohnort',
    'Bezahlt',
    'T-Shirt',
    'Fertig',
  );


  $i = 0;
  foreach ($head as $line) {
    $sheet->setCellValue(col($l, $i), $line);
    $i++;
  }

  foreach ($registrations as $row) {
    $l++;

    //Kontodaten
    if($row['paid'] === '0000-00-00') {
      $transfer = '----';
    } else {
      $transfer = date('d.m.Y', strtotime($row['paid']));
    }


    // Abgefertigt
    if($row['deliveried'] === '0000-00-00' || $row['deliveried'] == null) {
      $deliveried = 'Nein';
    } else {
      $deliveried = 'Ja';
    }

    $line = array(
      $row['key'],
      $row['ak'],
      $row['route'],
      $row['name'],
      $row['firstname'],
      $row['age'],
      $row['sex'],
      $row['city'],
      $transfer,
      $row['size'],
      $deliveried,
    );

    $i = 0;
    foreach ($line as $col) {
      $sheet->setCellValue(col($l, $i), $col);
      $i++;
    }
  }

  ob_end_clean();

  // Redirect output to a client’s web browser (Excel5)
  header('Content-Type: application/vnd.ms-excel');
  header('Content-Disposition: attachment;filename="'.$filename.'.xls"');

  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
  $objWriter->save('php://output');
  exit;

}
?>
<p>Bitte Strecke wählen:</p>
<form method="post">
<input type="hidden" value="1" name="export"/>
<input type="hidden" value="1" name="all"/>
<button type="submit">Alle</button>
</form>
<form method="post">
<input type="hidden" value="1" name="export"/>
<input type="hidden" value="1" name="250"/>
<button type="submit">250</button>
</form>
<form method="post">
<input type="hidden" value="1" name="export"/>
<input type="hidden" value="1" name="400"/>
<button type="submit">400</button>
</form>
<form method="post">
<input type="hidden" value="1" name="export"/>
<input type="hidden" value="1" name="1600"/>
<button type="submit">1600</button>
</form>
<form method="post">
<input type="hidden" value="1" name="export"/>
<input type="hidden" value="1" name="4000"/>
<button type="submit">4000</button>
</form>
