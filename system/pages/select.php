<div class="container">
<div class="five columns offset-by-two"><div id="edit_find_user"><a href="?page=swimmer_select" class="button">Angemeldeten Schwimmer auswählen</a></div></div>
<div class="five columns offset-by-two"><div id="list_add_user"><a href="?page=swimmer_new" class="button">Neuen Schwimmer anmelden</a></div></div>
</div>
<?php

$routes = array();
foreach($lss['routes'] as $route) {
  $routes[$route['db']] = $route;
  $routes[$route['db']]['registered'] = 0;
  $routes[$route['db']]['paid'] = 0;
  $routes[$route['db']]['deliveried'] = 0;
  $routes[$route['db']]["doppelt"] = 0;

  foreach (call_user_func("Registration::get".$route["db"]) as $row) {
    $routes[$route['db']]['registered']++;
    if ($row['paid'] != '0000-00-00') {
      $routes[$route['db']]['paid']++;
    }
    if ($row['deliveried'] != NULL) {
      $routes[$route['db']]['deliveried']++;
    }
    if ($row['route'] == "doppelt") {
      $routes[$route['db']]['doppelt']++;
    }
  }
}

echo '<div class="container">';
echo '<table class="sixten columns table" style="margin-top:20px;">';
echo '<tr><th>Strecke</th><th>Registriert</th><th>Bezahlt</th><th>Ausgehändigt</th><th>Maximal</th><th>Offen</th><th>Doppelt</th></tr>';
foreach ($routes as $route) {
  echo '<tr>',
    '<th>'.$route['name'].'</th>',
    '<td>'.$route['registered'].'</td>',
    '<td>'.$route['paid'].'</td>',
    '<td>'.$route['deliveried'].'</td>';

    if ($route['max'] != -1) {
      if ($route['registered'] < $route['max']) {
        echo '<td>';
      } else {
        echo '<td class="fail">';
      }
      echo $route['max'].'</td>';
    } else {
      echo '<td></td>';
    }
    echo
    '<td>'.($route['registered']-$route['deliveried']).'</td>';

    if ($route["doppelt"] > 0) {
      echo '<td>'.($route['doppelt']).'</td>';
    } else {
      echo '<td></td>';
    }
    echo
  '</tr>';
}
echo '</table></div>';


?>
<script type="text/javascript">
$(function(){
	$('#edit_find_user, #list_add_user')
		.click(function(){
			window.location = $(this).find('a').attr('href');
		})
		.css('cursor', 'pointer');
});
</script>
